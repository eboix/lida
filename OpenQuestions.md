## For MSH: relationship between LIDA (LIris DAta) and EDP ?
Concern: organizational simplicity and coherence bring readability to the project
  * EBO is part of the EDP team. Yet LIRIS data is not a platform to which EDP participates. How can that work ?
  * MSC counsels LIRIS head concerning EDP aspects. What is the exact role that MSC plays towards LIDA ?
  * To whom does LIRIS head delegate the LIDA project management ?
  * Why is LIRIS data platform not part of the other LIRIS platforms ?
  * Where is the LIRIS data organization chart ?
  * Is LIDA an object or a mission ? What is the relationship between LIDA and an "Action transverse" whose first mission would be to setup a shared platform that would realize a watch on city oriented services, technology, tools, data... ?

## What are the human resources ?
Concern: manpower is key. A team must be constituted and seeking benevolent contributors is the french academia free association way of doing things. Such benevolent might have different types of [ROI](https://en.wikipedia.org/wiki/Return_on_investment) that must be met (which folds back to describing their needs i.e. what they expect). Hence establishing the list of the contributors and their expectancy is a must for the "contract" effectiveness:
* On the ITA side of the force:
  * YPE is off LIDA to sysadmin and won't contribute anymore
  * …?
* On the researchers side of the force: bellow is the list of the participants to LIDA V1 that helped in defining the needs. Are they still "benevolent" or charged of anything ?
  * [Sylvie Servigne](http://liris.cnrs.fr/membres/?id=15) [BD](http://liris.cnrs.fr/equipes?id=61)
  * [Pierre Antoine Champin](http://liris.cnrs.fr/pierre-antoine.champin/en/) [TWEAK](https://liris.cnrs.fr/equipes/?id=75)
  * [Pierre Edouard Portier](http://liris.cnrs.fr/pierre-edouard.portier/) DRIM
  * [Mickael Mrissa](https://liris.cnrs.fr/membres/?id=1708) [SOC](https://liris.cnrs.fr/equipes?id=62)
  * Julien Milles left LIRIS

## Who are the LIDA users
Concern: LIDA start with very limited resources and there are many platforms/teams to satisfy. A "strategic", thus political, choice of resource allocation must be made.
  * Shouldn't we formulate the needs in terms of [software platforms](https://liris.cnrs.fr/plateformes-en) as opposed to research teams ?
  * Where is the teams/platforms need stated ? What are their current demand ?
  * For MSH: Which teams/platforms needs should be addressed ?
     * LIDA V1 initially targeted [Data Pole](https://liris.cnrs.fr/axes?id=68) and namely BD, DM2L and GOAL. Are these still the official "guilty parties"?
     * The [PLEID](http://liris.cnrs.fr/pleiad/) platform already has some support from a private company. Will the LIRIS further contribute to this software platform ?

## What is the LIDA mission statement
Concern: LIDA' objective is to satisfy research needs as opposed to be a resource provider. A clear mission statement would avoid LIDA being conceived as a storage provider (without pointing fingers [storing video](https://liris.cnrs.fr/equipes?id=48) require lots of disks) but as a platform/research enabler.

## What is the agenda?
Concern: be efficient and indulge the king baby syndrome (I want it and I want it now :-) )
  * What is the deadline for the white paper ? (MSC: December)

## Related question: visualization platform for city data (should be IMU related?)
When researchers work on city related data (e.g. on Grand Lyon data) they shall need:
  * to gain some understanding of the available data
  * to gain some understanding of their numerical results,
  * to present their results to non experts.
Such tasks prove harder when the concerned data is [geo-localized](https://en.wiktionary.org/wiki/geolocation), It could be that LIRIS misses helping tools for example a, possibly dedicated, virtual platform (SaS) allowing to display GIS and even 3D data (hopefully with web based thin clients or even plugin free web browser).

Since Grand Lyon (GL) would have the same concerns (display the results of the GL partners usage of the GL data) maybe this could be the role of GL, or IMU or even TUBA ?

In the meantime, some good practices should be documented and shared among LIRIS' researchers. For example for city oriented 3D data Gilles Gesquière (GGE) develops a viewer (FIXME: provide a pointer to 3D USE software) as well as algorithms and helpers (concerning formats and FME based ETL snippets). GGE also has at hand algorithms whose results are starting points or of central interest for other researchers (e.g. algorithms that provide topologically correct transportation networks when [Navteq](http://www.navmart.com/) data is still expensive and not always up to date).

Should LIRIS look for technologies (Cesium?) or partners (Oslandia) ?
