## CKAN with Solr-Jetty installation notes (failure) on Ubuntu 14

### Assumptions
The sudo command is effective

### Reference
We here follow the maintainers [installation guide](http://docs.ckan.org/en/latest/maintaining/installing/index.html).

### Installation per se
```
sudo apt-get install python-dev postgresql libpq-dev python-pip python-virtualenv git-core solr-jetty openjdk-6-jdk
```

The following block is not mandatory. It's purpose it to locates ckan installation in the (current user) home dir:
```
cd
mkdir -p ~/ckan/lib
sudo ln -s ~/ckan/lib /usr/lib/ckan
mkdir -p ~/ckan/etc
sudo ln -s ~/ckan/etc /etc/ckan
```

Create the ad-hoc python virtual environment
```
sudo mkdir -p /usr/lib/ckan/default
sudo chown `whoami` /usr/lib/ckan/default
virtualenv --no-site-packages /usr/lib/ckan/default
. /usr/lib/ckan/default/bin/activate
```

WARNING: from now on always make sure this python virtual environment is active.
Make sure by checking the bash prompt that should be prefixed with ``(default)` !

Install CKAN's current stable version and the required modules
```
pip install -e 'git+https://github.com/ckan/ckan.git@ckan-2.4.1#egg=ckan'
pip install -r /usr/lib/ckan/default/src/ckan/requirements.txt
```

Just making sure the `virtualenv` is ok:
```
deactivate
. /usr/lib/ckan/default/bin/activate
```

Create the PostgreSQL database `USER` named `ckan_default`
```
sudo -u postgres createuser -S -D -R -P ckan_default
```

Create the PostgreSQL `DATABASE` named `ckan_default`
```
sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
```

Create a `ckan` customization file...
```
sudo mkdir -p /etc/ckan/default
sudo chown -R `whoami` /etc/ckan/
cd /usr/lib/ckan/default/src/ckan
paster make-config ckan /etc/ckan/default/development.ini
```

...and customize ckan
```
sudo vim /etc/ckan/default/development.ini
   ---> sqlalchemy.url : change the "pass" string for the password of ckan_default user
   ---> ckan.site_url = http://liris-vm-32.insa-lyon.fr
```

Configure `jetty` (used under the hood by `Solr`)
```
sudo vim /etc/default/jetty
  ---> NO_START=0            # (line 4)
  ---> JETTY_HOST=127.0.0.1  # (line 15)
  ---> JETTY_PORT=8983       # (line 18)
```

Fix the broken launcher as [documented here](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=719875)
```
sudo vim /etc/init.d/jetty
  ---> ROTATELOGS=/usr/bin/rotatelogs      # (line 201)
```

Launch `jetty` and assert `jetty` is running and OK:
```
sudo service jetty start
sudo apt-get install lynx
lynx http://localhost:8983
```

And now assert that `Solr` is served properly by Jetty:
```
lynx http://localhost:8983/solr/
```
Now `Solr-jetty` fails poorly with the following message:
```
    HTTP ERROR 500
    Problem accessing /solr/index.jsp. Reason:
    JSP support not configured
```

### Inquiring on the reasons for such a failure
In order to inquire why let us retrieve the installed version number of jetty by following
[this method](https://wiki.eclipse.org/Jetty/FAQ#How_do_I_know_which_version_of_Jetty_I_am_running.3F).
Let's start by finding out where `start.java` is installed:
```
dpkg -l | grep -i jetty
dpkg -L libjetty-extra libjetty-extra-java libjetty-java solr-jetty jetty | grep -i start.jar
```
which thus happens to be ``/usr/share/jetty/``. We can now retrieve the jetty version:
```
pushd /usr/share/jetty/;  java -jar start.jar --version; popd
   ---> null 6.1.24
```
And BTW version 6 of jetty seems to be completely outdated (search for the word papyrus
[on this page](https://github.com/ckan/ckan/issues/1651).

With this version number google tells us that the above `Solr-jetty` error message seems to be a classic ubuntu distribution/packaging problem documented in the numerous following pages:
  * [Stackoverflow](http://stackoverflow.com/questions/30355839/ckan-local-installation-500-error-on-solr-jsp-support-not-configured)
  * [UbuntuForums](http://ubuntuforums.org/showthread.php?t=1380876)
  * [other related entry](http://stackoverflow.com/questions/3521654/missing-jsp-support-in-jetty-or-confusing-log-message)

Eventually note that [this CKAN Github hosted discussion](https://github.com/ckan/ckan/issues/1651) seems to mention that there is a workaround for this `solr-jetty` bug which consists in declaring `/usr/share/java/tomcat-coyote.jar` in the jetty config file `/etc/jetty/start.config`. Alas this failed for me...
