## Data Management Plan (DMP)

This document summarizes the various fields that are most often encountered within a [Data Management Plan](https://en.wikipedia.org/wiki/Data_management_plan) (DMP).

### Dataset reference and name
### Data set name and high level description
### Standards and Metadata: Data origin and content
  * How will the data be obtained? (created or collected among existing data)
  * Can any of this information be created automatically?
  * What metadata content standards will you use and why?
    * Intellectual content: title, summary, domain, keywords, resource type
    * Production content: date, creator (affiliation), project (goal), financing bodies, production means (where, acquisition/production instrument) What standards or methodologies will you use for obtaining them? (e.g. when collecting describe from which source, with which filters, cleanup methods...)
    * Technical characteristics: format, volumetry, files, structure/organisation
    * IP: ownership, access rights, sharing rights, usage rights...
  * What metadata format ? (refer to [Digital Curation Center a.k.a. DCC](http://www.dcc.ac.uk/))
    * Interdisciplinary e.g. [Dublin Core](http://dublincore.org/metadata-basics/), disciplinary e.g. SBML, EML, [CMDI](http://www.clarin.eu/content/component-metadata)
  * Reference: [Standards and Metadata](http://www.inist.fr/formations/DMP-Standards-et-metadonnees/story_html5.html)
  * Note: What about using the [Digital Object Identifier (DOI)](https://en.wikipedia.org/wiki/Digital_object_identifier)?

### Data description
  * Type of Data: source (refer above on data source), form, stability (static, incremental, substitutive), volumetry
  * Which data formats? Opening (read), encoding (write), compression.
  * How will you structure and name your folders and files? Naming conventions, folder structures (renaming tools exist)
  * How will you ensure that different versions of a data set are easily identifiable? (version control)
  * Reference: [Data description](http://www.inist.fr/formations/DMP-Description-des-donnees/story_html5.html)

### Data sharing: immediate diffusion (focuses on direct and rich usage of data)
  * Storage (commit/write):
    * Which type of repository? (institutional, standard repository for the discipline...)
    * Where (i.e. in which repository) will the data be deposited?
    * Commit rights (who has the writing right access)
  * Access (How will you make the data available to others?)
    * Tools and means for retrieval and usage (software like data format readers, algorithms, analysis protocols...)
    * [Digital Object Identifier (DOI)](https://en.wikipedia.org/wiki/Digital_object_identifier)
    * Accessing rights (who can retrieve data, are they embargoes...)
  * Usage (licenses: with whom will you share the data, and under what conditions?):
    * access rights, mining rights, exploitation, reproduction, diffusion
    * Can be dealt with [Creative Commons](https://creativecommons.org) licenses like [CCO](https://creativecommons.org/publicdomain/zero/1.0/) or [CC BY](http://creativecommons.org/licenses/by/4.0/)
  * Reference: [Data sharing](http://www.inist.fr/formations/DMP-Partage-des-donnees/story_html5.html)

### Archiving and preservation: focuses on mid to long term access (storage and backup)
  * What is the long-term preservation plan for the dataset? e.g. deposit in a data repository
  * Will additional resources be needed to prepare data for deposit or meet charges from data repositories?
  * Is additional specialist expertise (or training for existing staff) required?
  * Do you have sufficient storage and equipment or do you need to cost in more?
  * Will charges be applied by data repositories?
  * Have you costed in time and effort to prepare the data for sharing / preservation?
  * Notes:
    * In France [CINES'](https://www.cines.fr/) vocation is to offer archiving services
    * Standards: METS (Metadata Encoding and Transmission Standard) and PREMIS (Preservation metadata: implementation strategies)
    * Data repositories are usually concerned with diffusion but not with archiving
    * [OAIS (Open Archival Information System)](https://en.wikipedia.org/wiki/Open_Archival_Information_System) normalizes (ISO) the archival systems and [describes](https://en.wikipedia.org/wiki/Open_Archival_Information_System#/media/File:OAIS-.gif): ingestion (input to the system), access (outputs of the system), management, storage...
  * Reference: [Archiving and preservation](http://www.inist.fr/formations/DMP-Archivage-des-donnees/story_html5.html)

### Data discoverability
  * Are the data and associated software produced and/or used in the project discoverable (and readily located), identifiable by means of a standard identification mechanism (e.g. Digital Object Identifier)?
  * How will potential users find out about your data?
  * Will you provide metadata online to aid discovery and reuse?

### Data accessibility
Are the data and associated software produced and/or used in the project accessible and in what modalities, scope, licenses?

### Data assessability and intelligibility
Are the data and associated software produced and/or used in the project assessable for and intelligible to third parties in contexts such as scientific scrutiny and peer review?

### Reusability
Are the data and associated software produced and/or used in the project useable by third parties even long time after the collection of the data?

### Interoperability to specific quality standards
Are the data and associated software produced and/or used in the project interoperable allowing data exchange between researchers, institutions, organizations, countries, etc?

### Sources
Content of this document was manually extracted from [DMPOnline tool](https://dmponline.dcc.ac.uk).
Note that doing taking into account the [DMPTool](https://dmptool.org/) site content proved to be harder since DMPTool is customized with disciplinary (scientific domain) oriented DMP templates.
