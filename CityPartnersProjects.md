Some current data project of LIRIS partners
* [Data Grand Lyon](http://data.grandlyon.com/) is the open data of [Grand Lyon](http://www.grandlyon.com/)
* [Amadouer](http://liris.cnrs.fr/~sservign/Amadouer.html) project
  * Data is mainly thermal measures at the room level within buildings.
  * Funding from [CNRS Mastodons](http://www.cnrs.fr/mi/spip.php?article53&lang=fr) (Grande MASse de DONnees
  * LIRIS-INSA is a partner
  * Active since 2012
  * Some software (web based building presentation and treatment workflow editor/player) was developed: refer to pages 12 and 15 of [2014 presentation](http://liris.cnrs.fr/~sservign/AmadouerMastodons012014.pdf)
