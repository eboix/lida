# LIDA V1.1

### Previous LIDA version
All the information related to the LIDA project is still located
on [this Doku wiki](https://projet.liris.cnrs.fr/lirisdat/wiki/doku.php) 

### Project holders:
  * Head of LIRIS
  * [Eric Boix](https://liris.cnrs.fr/membres?idn=eboix)
  * [Emmanuel Coquery](https://liris.cnrs.fr/membres?idn=ecoquery)
  * Who else ? refer to [open question](OpenQuestions.md)

### Various immature pages:
  * Informal [LIRIS Data inventory](/DataUsedLiris.md)
  * [Proposed activities](/ProjectPropositions.md)
  * [Open questions](/OpenQuestions.md)
  * [DMP](/DataManagementPlan.md) (Data Management Plan)
  * City domain:
    * [Partners and projects](/CityPartnersProjects.md)
    * [City oriented data Tools](/CityDataTool.md)

### Technologies:
  * [CKAN](https://ckan.org/):
     * [Solr-Tomcat successful install from sources on Ubuntu](/CkanUbuntuTomcatInstall.md)
     * [Solr-Jetty fails to install on Ubuntu](/CkanUbuntuJettyInstallFailure.md)
  * [Knime](https://tech.knime.org/installation-0)
    * Open source (GPL), German made
    * [Installation out of sources](https://tech.knime.org/community/developers)
