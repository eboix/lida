## Data a priori used at LIRIS
How to **improve this page**:
  * separate (when the information is known) the data that is used (remote download at runtime) from data that is hosted in a database at LIRIS (after harvesting, replication campaign)
  * Fix the many FIXME that appear when there is missing information concerning the data.

## Used / Available data
  * [LIDAR](https://en.wikipedia.org/wiki/Lidar) data, 3D buildings... FIXME
    * Provider: [Data Grand Lyon](http://data.grandlyon.com/) (DGL, the open data of [Grand Lyon](http://www.grandlyon.com/))
    * Users: [Gilles Gesquière](http://liris.cnrs.fr/gilles.gesquiere/) in person ?
    * Upstream access: Specific license with GL (concerning non public data)
    * Source of info: [Gilles Gesquière](http://liris.cnrs.fr/gilles.gesquiere/)

  * [Velo'v](http://www.velov.grandlyon.com/?L=1) [Data Grand Lyon]](http://data.grandlyon.com/) data FIXME
    * Keywords: real data, traffic
    * Provider: [Data Grand Lyon](http://data.grandlyon.com/)
    * Contact: [Celine Robardet](http://liris.cnrs.fr/celine.robardet/)
    * Users: [DM2L team](http://liris.cnrs.fr/dm2l/)
    * Upstream access: special license with GL
    * Local access: ?
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: is this plain usage or hosted at LIRIS ?

  * Harvested streamed data of [Data Grand Lyon](http://data.grandlyon.com/) traffic data: FIXME
    * Keywords: real data, traffic
    * Provider: [Data Grand Lyon](http://data.grandlyon.com/)
    * Contact: [Marian Scuturici](http://liris.cnrs.fr/vasile-marian.scuturici)
    * Users: ???
    * Access: special license with GL (upstream), restricted (re-diffusion)
    * Source of info: [Marian Scuturici](http://liris.cnrs.fr/vasile-marian.scuturici/)
    * Open questions: where, what, how is the data ?

  * Traffic data from [Data Grand Lyon](http://data.grandlyon.com/): FIXME
    * Keywords: real data, PV street data
    * Provider: [PC CRITER](http://www.economie.grandlyon.com/actualite-economie-actu-lyon.194+M540e34de1a7.0.html)
    * Contact: [Christine Solnon](http://liris.cnrs.fr/christine.solnon/)
    * Size: 2 samples every six minutes over 600 street sampling points
    * Access: Specific license with GL Optimod project members (upstream), account secured on LIDA
    * Projects: [Optimod](http://www.optimodlyon.com/en/) and it's sequel ASTRAL  
    * Data: (re) hosted at LIDA
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey

  * [Data Grand Lyon](http://data.grandlyon.com/) transportation data: FIXME
    * Keywords: real data, Public Transportation
    * Provider: [Data Grand Lyon](http://data.grandlyon.com/)
    * Contact: [Celine Robardet](http://liris.cnrs.fr/celine.robardet/)
    * Users: [DM2L team](http://liris.cnrs.fr/dm2l/)
    * Upstream access: special license with GL
    * Local access: ?
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: is this plain usage or hosted at LIRIS ?

  * [Grizzly](http://www.hikob.com/wp-content/uploads/2015/06/HIKOB_Case_Studies_GrandLyon.pdf) harvested data: FIXME
    * Keywords: real data, street temperature
    * Provider: [Data Grand Lyon](http://data.grandlyon.com/)
    * Contact: [Marian Scuturici](http://liris.cnrs.fr/vasile-marian.scuturici/)
    * Users: ???
    * Access: ???
    * Source of info: [Marian Scuturici](http://liris.cnrs.fr/vasile-marian.scuturici/)
    * Open questions: where, what, how is the data ?

  * Streaming video: FIXME
    * Keywords: real time, real data, streaming, video
    * Provider: [Foxstream company](http://www.foxstream-intelligencevideo.com/)
    * Contact: [Marian Scuturici](http://liris.cnrs.fr/vasile-marian.scuturici/)
    * Users:  [Database Group (aka BD)](https://liris.cnrs.fr/equipes?id=61) and [DM2L team](http://liris.cnrs.fr/dm2l/)
    * Access: restricted
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: where, what, how is the data ?

  * Energy sensors: FIXME
    * Keywords: cloud-based services, energy data streams, semi-structured data
    * Contacts: [SOC team](https://liris.cnrs.fr/equipes?id=62), [Parisa Ghodous](http://liris.cnrs.fr/parisa.ghodous), [Catarina Ferreira da Silva](http://liris.cnrs.fr/catarina.ferreira-da-silva/)
    * Access: restricted
    * Volume: high
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: where, what, how is the data ?

  * Constrained synthetic multidimensional data: FIXME
    * Contacts: [Database Group (aka BD)](https://liris.cnrs.fr/equipes?id=61); [Stéphane Coulondre](http://stephane.coulondre.info/), [Maryvonne Miquel](https://liris.cnrs.fr/membres/?id=5), [Anne Tchounikine](http://liris.cnrs.fr/anne.tchounikine/)
    * Volume: high
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: where, what, how is the data ?

  * Soq4Home: Building level temperature sensor FIXME
    * Contact: Contacts: [Database Group (aka BD)](https://liris.cnrs.fr/equipes?id=61); [Anne Tchounikine](http://liris.cnrs.fr/anne.tchounikine/) ???
    * Owner: LIRIS
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: where is the data how to access it...

  * Hand pose / fine Grained Gestures FIXME
    * Keywords: real data, video ?
    * Owner: LIRIS
    * Contact: [Christian Wolf](http://liris.cnrs.fr/christian.wolf/), [Christophe Garcia](http://christophegarciafr.wix.com/home-page)
    * Users: [IMAGINE team](http://liris.cnrs.fr/imagine/)
    * Local access: local and restricted to the team
    * Data form: database
    * Size: 36Go
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: how is this hosted at LIRIS ?

  * [ChaLearn](http://gesture.chalearn.org/2014-looking-at-people-challenge) gesture data FIXME  
    * Keywords: real data, video ?
    * Provider: [ChaLearn 2014](http://gesture.chalearn.org/2014-looking-at-people-challenge)
    * Contact: [Christian Wolf](http://liris.cnrs.fr/christian.wolf/), [Christophe Garcia](http://christophegarciafr.wix.com/home-page)
    * Users: [IMAGINE team](http://liris.cnrs.fr/imagine/)
    * Local access: ?
    * Size: 26Go for input data, 2To for intermediate computation results
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: how is this hosted at LIRIS ?

  * [ImageNet](http://www.image-net.org/) images FIXME  
    * Keywords: real data, video ?
    * Provider: [ImageNet](http://www.image-net.org/)
    * Contact: [Christian Wolf](http://liris.cnrs.fr/christian.wolf/), [Christophe Garcia](http://christophegarciafr.wix.com/home-page)
    * Users: [IMAGINE team](http://liris.cnrs.fr/imagine/)
    * Local access: ?
    * Size: 125Go
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
    * Open questions: how is this hosted at LIRIS ?

  * MARBRE
    * Data first collected with the [Amadouer project](http://liris.cnrs.fr/~sservign/Amadouer.html)
    * Used within the [SocQ4Home](http://liris.cnrs.fr/socq4home/site/) project
    * Keywords: building, temperature, door/window aperture, meteorological data
    * Contact: Yann Gripay

## Also mentioned
  * The [PetaSky](http://com.isima.fr/Petasky) project (contact for the data [Mohand-Said Hacid](http://perso.univ-lyon1.fr/mohand-said.hacid/)) seems to use data intensely...
  * [Emmanuel Coquery](http://liris.cnrs.fr/ecoquery/dokuwiki/doku.php) uses some "toy data sampling" on teaching purposes ([Bdav](http://liris.cnrs.fr/ecoquery/dokuwiki/doku.php?id=enseignement:bdav:start)).
  * [Celine Robardet](http://liris.cnrs.fr/celine.robardet/) mentioned (within [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey) without further precision on data nature, source...that [DM2L team](http://liris.cnrs.fr/dm2l/) makes usage of:
    * USA flight data
    * Satellite imagery
    * Biological medical data
  * [Fabien de Marchi](http://liris.cnrs.fr/fabien.demarchi/) made some usage of a panel of [CNRL](https://crnl.univ-lyon1.fr/index.php/) data within a now (2015) terminated LIRIS "transverse action" ([Olfaminer](https://liris.cnrs.fr/equipes/?id=61&onglet=conv))
  * [Prima'Mov](http://liris.cnrs.fr/privamov/project/) project (contact [Sonia Ben Mokhtar](https://sites.google.com/site/soniabm/)) supposedly collects (at LIRIS?) mobility traces.

## Deprecated / Lost data
  * Indexation of Decentralized/Distributed Data Warehouses (Grid)
    * Technology: [OLAP](https://en.wikipedia.org/wiki/Online_analytical_processing)/[GLOBUS](https://www.globus.org/)
    * Contacts: [Database Group (aka BD)](https://liris.cnrs.fr/equipes?id=61); [Maryvonne Miquel](https://liris.cnrs.fr/membres/?id=5), [Anne Tchounikine](http://liris.cnrs.fr/anne.tchounikine/)
    * Data: lost on project termination
    * Project: [Grille Géno Médicale (GGM)](http://acimd.labri.fr/04FICHES/GGM.htm)
    * Source of info: [Philippe Lamarre](http://www.philippe-lamarre.fr/)'s Big Data at LIRIS survey
